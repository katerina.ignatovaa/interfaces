import {User} from "../interfaces/users"
import {UserInfo, Organization} from "../interfaces/usersInfo"
import {usersArray} from "./users";
import {usersInfoArray} from "./userInfo";

type newUser = Omit<User, 'userid'> & Pick<UserInfo, 'age'> &  Pick<Organization, 'position'>

function getUsersJobPositions(usersArray:User[]):newUser[]{
    const newUsersArray = [];
    usersArray.forEach(
        (user) =>{
            delete user.userid;
            const userInfo = usersInfoArray.find(
                (userInfo) => userInfo.name === user.name
            )
            newUsersArray.push(Object.assign(user, {age: userInfo.age, position: userInfo.organization.position}))
        }
    )
    return newUsersArray;
}

const usersPositions = getUsersJobPositions(usersArray);

console.log('userPositions', usersPositions)

