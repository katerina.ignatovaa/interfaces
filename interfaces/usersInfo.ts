export interface Organization{
    name: string,
    position: string
}

export interface UserInfo{
    userid: string,
    name: string,
    birthdate: Date,
    age: number,
    organization: Organization
}
