export interface User{
    userid: string,
    name: string,
    gender: 'man' | 'woman'
}